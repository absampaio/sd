package Trabalho1;

/**
 * Created by jpf on 3/19/15.
 */
public class ServerInfo implements java.io.Serializable {

    String name;
    String address;

    public ServerInfo(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
