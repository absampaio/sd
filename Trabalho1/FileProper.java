package Trabalho1;

import java.util.Date;

/**
 * Created by jpf on 3/9/15.
 */
public class FileProper extends FileInfo {

    byte[] data;

    public FileProper(String name, long length, Date modified, boolean isFile, byte[] data) {
        super(name, length, modified, isFile);
        this.data = data;
    }

    public void saveFile(String pathname) {
        System.out.println(data.length);
    }
}
