package Trabalho1;

import java.nio.file.Files;
import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.util.*;
import java.io.*;

public class DirServerImpl extends UnicastRemoteObject implements IFileServer
{
	protected DirServerImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;

	private String basePathName;
	private File basePath;

	protected DirServerImpl( String pathname) throws RemoteException {
		super();
		this.basePathName = pathname;
		basePath = new File( pathname);
	}

	@Override
	public String[] dir(String path) throws RemoteException, InfoNotFoundException {
		File f = new File( basePath, path);
		if( f.exists())
			return f.list();
		else
			throw new InfoNotFoundException( "Directory not found :" + path);
	}
	
	public void makeDir (String path) throws RemoteException, InfoNotFoundException{
		File dir = new File (basePath, path);
		if (!dir.exists()){
			dir.mkdirs();
		} else throw new InfoNotFoundException( "Directory found at :" + path);
	}
	
	public void removeDir(String path) throws RemoteException, InfoNotFoundException { 
		File f = new File( basePath, path); 
		if(!f.delete()) throw new InfoNotFoundException( "Directory not found :" + path); 
	}
	
	@Override
	public void removeFile(String path) throws RemoteException, InfoNotFoundException {
		File f = new File( basePath, path); 
		if(!f.delete()) throw new InfoNotFoundException( "File not found :" + path); 
		
	}
	
	@Override
	public String[] listFiles(String path) throws RemoteException, InfoNotFoundException {
		File folder = new File(basePath, path);
		File[] listOfFiles = folder.listFiles();
		String [] fileNames = new String [listOfFiles.length];
		for (int i = 0; i < listOfFiles.length; i++){
			fileNames[i] = listOfFiles[i].getName();
		}
		return fileNames;
	}

	@Override
	public FileInfo getFileInfo(String path) throws RemoteException, InfoNotFoundException {
		File f = new File( basePath, path);
			if( f.exists()){
				return new FileInfo( path, f.length(), new Date(f.lastModified()), f.isFile());
		} else
			throw new InfoNotFoundException( "Directory/File not found :" + path);
	}

    @Override
    public FileProper getFile(String path) throws RemoteException, InfoNotFoundException {
        File f = new File( basePath, path);
            if( f.exists())
                try{
                    byte[] data = Files.readAllBytes(f.toPath());
                    return new FileProper( path, f.length(), new Date(f.lastModified()), f.isFile(), data);
                } catch(IOException e) {
                    throw new RemoteException("Failed to get File");
                }
            else
                throw new InfoNotFoundException( "File not found :" + path);
    }

    public void putFile(String path, byte[] data) throws RemoteException, InfoNotFoundException {
    	File f = new File( basePath, path);
        if( !f.exists())
            try{
                if (f.createNewFile()){
                		FileOutputStream fos = new FileOutputStream(f);
                    	fos.write(data);
                    	fos.close();
                }
                else
                    throw new InfoNotFoundException( "Failed to create file :" + path);
            } catch(IOException e) {
                throw new RemoteException("Failed to put File");
            }
        else
            throw new InfoNotFoundException( "File found :" + path);
    }

    public static void main( String args[]) throws Exception {
		try {
			String path = ".";
			if( args.length < 2 || args.length > 3){
                System.out.println("USE: server_name contact_server_url root_path");
                System.exit(1);
			} else if (args.length == 3){
				path = args[2];
			}

            IContactServer s = (IContactServer) Naming.lookup("//" + args[1] + "/contactServer");
            s.register(args[0], "localhost");

			System.getProperties().put( "java.security.policy", "src/policy.all");

			if( System.getSecurityManager() == null) {
				System.setSecurityManager( new RMISecurityManager());
			}

			DirServerImpl server = new DirServerImpl(path);
			Naming.rebind( args[0], server);

			System.out.println( "DirServer bound in registry");
		} catch( Throwable th) {
			th.printStackTrace();
		}
	}



	

	


}
