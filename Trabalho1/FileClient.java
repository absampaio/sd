package Trabalho1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.URL;
import java.nio.file.Files;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Random;

import javax.xml.namespace.QName;

import aula3.clt.ws.FileServerImplWS;
import aula3.clt.ws.FileServerImplWSService;

/**
 * Classe base do cliente
 * @author nmp
 */
public class FileClient
{
    String contactServerURL;

    protected FileClient( String url) {
        this.contactServerURL = url;
    }


    /**
     * Devolve um array com os servidores a correr caso o name== null ou o URL dos
     * servidores com nome name.
     */
    protected String[] servers( String name) {
        try{
            IContactServer s = (IContactServer) Naming.lookup("//" + contactServerURL + "/contactServer");
            return s.getServers(name);
        }catch (InfoNotFoundException e){
            System.err.println( "Name not found");
            return new String [0];
        }catch (RemoteException e) {
            System.out.println("Connection Error");
            e.printStackTrace();
            return new String [0];
        }catch (Exception e) {
            e.printStackTrace();
            return new String [0];
        }
    }

    private String getServerIp (String server){
        String [] s = this.servers(server);
        Random rand = new Random();
        int  n = rand.nextInt(s.length);
        return s[n];
    }
    
    
    
    private boolean isWebService(String url){
    	boolean isWs = false;
    	if (url.toLowerCase().startsWith("http")){
    		isWs = true;
    	}
    	return isWs;
    }

    /**
     * Devolve um array com os ficheiros/directoria na directoria dir no servidor server
     * (ou no sistema de ficheiros do cliente caso server == null).
     * Se isURL for verdadeiro, server representa um URL para o servior (e.g. //127.0.0.1/myServer).
     * Caso contrario e o nome do servidor. Nesse caso deve listar os ficheiro dum servidor com esse nome.
     * Devolve null em caso de erro.
     * NOTA: nao deve lancar excepcao.
     */
    protected String[] dir( String server, boolean isURL, String dir) {

        try{
        	IFileServer s = null;
        	FileServerWS server = null;
            if (server!= null){
            	if (isURL){
            		if (isWebService(server)){
            			FileServerWSService service = new FileServerWSService( new URL( server + "?wsdl"), new QName(server, "FileServerWSService"));
            			FileServerWS server = service.getFileServerWSPort();
            		} else {
            			s = (IFileServer) Naming.lookup(server);
            		}
            	} else {
            		 String address = getServerIp(server);
            		 if (isWebService(address)){
            			//TODO
             		} else {
             			s = (IFileServer) Naming.lookup("//" + address + "/"+server);
             		}
            	}
            	return s == null ? s.listFiles(dir) : server.listFiles(dir);
            } else{
                return listFiles(dir);
            }
        }catch (Exception e){
            System.err.println( "exec: ls " + dir + " no servidor " + server + " - e url : " + isURL);
            return new String [0];
        }
    }

    private String[] listFiles (String dir) throws InfoNotFoundException{
        File f = new File(dir);
        if( f.exists())
            return f.list();
        else
            throw new InfoNotFoundException( "Directory not found :" + dir);
    }

    /**
     * Cria a directoria dir no servidor server@user
     * (ou no sistema de ficheiros do cliente caso server == null).
     * Se isURL for verdadeiro, server representa um URL para o servior (e.g. //127.0.0.1/myServer).
     * Caso contrario e o nome do servidor. Nesse caso deve listar os ficheiro dum servidor com esse nome.
     * Devolve false em caso de erro.
     * NOTA: nao deve lancar excepcao.
     * @throws RemoteException
     */
    protected boolean mkdir( String server, boolean isURL, String dir) throws RemoteException {

    	try{
        	IFileServer s = null;
        	FileServerWS server = null;
            if (server!= null){
            	if (isURL){
            		if (isWebService(server)){
            			FileServerWSService service = new FileServerWSService( new URL( server + "?wsdl"), new QName(server, "FileServerWSService"));
            			FileServerWS server = service.getFileServerWSPort();
            		} else {
            			s = (IFileServer) Naming.lookup(server);
            		}
            	} else {
            		 String address = getServerIp(server);
            		 if (isWebService(address)){
            			//TODO
             		} else {
             			s = (IFileServer) Naming.lookup("//" + address + "/"+server);
             		}
            	}
            	return s == null ? s.makeDir(dir) : server.makeDir(dir);
            } else{
                makeDir(dir);
            }
            return true;
        }catch (Exception e){
            System.err.println( "exec: mkdir " + dir + " no servidor " + server +" - e url : " + isURL);
            return false;
        }

    }

    private void makeDir (String path) throws InfoNotFoundException{
        File dir = new File (path);
        if (!dir.exists()){
            dir.mkdirs();
        } else throw new InfoNotFoundException( "Directory found at :" + path);
    }

    /**
     * Remove a directoria dir no servidor server@user
     * (ou no sistema de ficheiros do cliente caso server == null).
     * Se isURL for verdadeiro, server representa um URL para o servior (e.g. //127.0.0.1/myServer).
     * Caso contrario e o nome do servidor. Nesse caso deve listar os ficheiro dum servidor com esse nome.
     * Devolve false em caso de erro.
     * NOTA: nao deve lancar excepcao.
     */
    protected boolean rmdir( String server, boolean isURL, String dir) {
    	try{
        	IFileServer s = null;
        	FileServerWS server = null;
            if (server!= null){
            	if (isURL){
            		if (isWebService(server)){
            			FileServerWSService service = new FileServerWSService( new URL( server + "?wsdl"), new QName(server, "FileServerWSService"));
            			FileServerWS server = service.getFileServerWSPort();
            		} else {
            			s = (IFileServer) Naming.lookup(server);
            		}
            	} else {
            		 String address = getServerIp(server);
            		 if (isWebService(address)){
            			//TODO
             		} else {
             			s = (IFileServer) Naming.lookup("//" + address + "/"+server);
             		}
            	}
            	return s == null ? s.removeDir(dir) : server.removeDir(dir);
            } else {
                removeDir(dir);
            }
            return true;
        }catch (Exception e){
            System.err.println( "exec: rmdir " + dir + " no servidor " + server +" - e url : " + isURL);

            return false;
        }
    }

    private void removeDir (String path) throws InfoNotFoundException{
        File f = new File (path);
        if(!f.delete()) throw new InfoNotFoundException( "Directory not found :" + path);
    }

    /**
     * Remove o ficheiro path no servidor server@user.
     * (ou no sistema de ficheiros do cliente caso server == null).
     * Se isURL for verdadeiro, server representa um URL para o servior (e.g. //127.0.0.1/myServer).
     * Caso contrario e o nome do servidor. Nesse caso deve listar os ficheiro dum servidor com esse nome.
     * Devolve false em caso de erro.
     * NOTA: nao deve lancar excepcao.
     */
    protected boolean rm( String server, boolean isURL, String path) {
    	try{
        	IFileServer s = null;
        	FileServerWS server = null;
            if (server!= null){
            	if (isURL){
            		if (isWebService(server)){
            			FileServerWSService service = new FileServerWSService( new URL( server + "?wsdl"), new QName(server, "FileServerWSService"));
            			FileServerWS server = service.getFileServerWSPort();
            		} else {
            			s = (IFileServer) Naming.lookup(server);
            		}
            	} else {
            		 String address = getServerIp(server);
            		 if (isWebService(address)){
            			//TODO
             		} else {
             			s = (IFileServer) Naming.lookup("//" + address + "/"+server);
             		}
            	}
            	return s == null ? s.removeFile(path) : server.removeFile(path);
            } else {
                removeFile(path);
            }
            return true;
        }catch (Exception e){
            System.err.println( "exec: rm " + path + " no servidor " + server +" - e url : " + isURL);

            return false;
        }
    }

    private void removeFile (String path) throws InfoNotFoundException{
        File f = new File (path);
        if(!f.delete()) throw new InfoNotFoundException( "File not found :" + path);
    }

    /**
     * Devolve informacao sobre o ficheiro/directoria path no servidor server@user.
     * (ou no sistema de ficheiros do cliente caso server == null).
     * Se isURL for verdadeiro, server representa um URL para o servior (e.g. //127.0.0.1/myServer).
     * Caso contrario e o nome do servidor. Nesse caso deve listar os ficheiro dum servidor com esse nome.
     * Devolve false em caso de erro.
     * NOTA: nao deve lancar excepcao.
     */
    protected FileInfo getAttr( String server, boolean isURL, String path) {

    	try{
        	IFileServer s = null;
        	FileServerWS server = null;
            if (server!= null){
            	if (isURL){
            		if (isWebService(server)){
            			FileServerWSService service = new FileServerWSService( new URL( server + "?wsdl"), new QName(server, "FileServerWSService"));
            			FileServerWS server = service.getFileServerWSPort();
            		} else {
            			s = (IFileServer) Naming.lookup(server);
            		}
            	} else {
            		 String address = getServerIp(server);
            		 if (isWebService(address)){
            			//TODO
             		} else {
             			s = (IFileServer) Naming.lookup("//" + address + "/"+server);
             		}
            	}
            	return s == null ? s.getFileInfo(path) : server.getFileInfo(path);
            } else {
                return getFileInfo(path);
            }

        }catch (Exception e){
            System.err.println( "exec: getattr " + path +  " no servidor " + server + " - e url : " + isURL);
            return null;
        }
    }

    private FileInfo getFileInfo(String path) throws InfoNotFoundException {
        File f = new File(path);
        if( f.exists()){
            return new FileInfo( path, f.length(), new Date(f.lastModified()), f.isFile());
        } else
            throw new InfoNotFoundException( "Directory/File not found :" + path);
    }

    /**
     * Copia ficheiro de fromPath no servidor fromServer@fromUser para o ficheiro
     * toPath no servidor toServer@toUser.
     * (caso fromServer/toServer == local, corresponde ao sistema de ficheiros do cliente).
     * Devolve false em caso de erro.
     * NOTA: nao deve lancar excepcao.
     */
    protected boolean cp( String fromServer, boolean fromIsURL, String fromPath,
                          String toServer, boolean toIsURL, String toPath) {
        try{
        	IFileServer from = null;
            if (fromServer != null){
            	if (fromIsURL){
            		if (isWebService(fromServer)){
            			//TODO
            		} else {
            			from = (IFileServer) Naming.lookup(fromServer);
            		}
            	} else {
            		 String addressFrom = getServerIp(fromServer);
            		 if (isWebService(addressFrom)){
            			//TODO
            		 } else {
            			 from = (IFileServer) Naming.lookup("//" + addressFrom + "/"+fromServer);
            		 }  
            	}
                FileProper tmp = from.getFile(fromPath);
                IFileServer to = null;
                if (toServer != null){
                	if (toIsURL){
                		if (isWebService(toServer)){
                			//TODO
                		} else {
                			to = (IFileServer) Naming.lookup(toServer);
                		}
                		
                	} else {
                		String addressTo = getServerIp(toServer);
                		if (isWebService(addressTo)){
                			//TODO
                		}else {
                			 to = (IFileServer) Naming.lookup("//" + addressTo + "/"+toServer);
                		}
                       
                	}
                    to.putFile(toPath, tmp.data);
                } else {
                    File f = new File(toPath);
                    if( !f.exists()){
                        try{
                            if (f.createNewFile()){
                                FileOutputStream fos = new FileOutputStream(f);
                                fos.write(tmp.data);
                                fos.close();
                            }
                            else
                                throw new InfoNotFoundException( "Failed to create file :" + toPath);
                        } catch (Exception e){}
                    }
                }
            }else {
            	IFileServer s = null;
                if (toServer != null){
                	if (toIsURL){
                		if (isWebService(toServer)){
                			//TODO
                		}else {
                			s = (IFileServer) Naming.lookup(toServer);
                		}
                	} else {
                		 String addressTo = getServerIp(toServer);
                		 if (isWebService(addressTo)){
                			 
                		 }else {
                			 s = (IFileServer) Naming.lookup("//" + addressTo + "/" + toServer);
                		 }
                	}
                    RandomAccessFile file = new RandomAccessFile(fromServer, "r");
                    byte[] b = new byte[(int)file.length()];
                    file.read(b);
                    s.putFile(toPath,b);
                } else {
                    copy(fromPath,toPath);
                }
            }	return true;
        }catch (Exception e){
            System.err.println( "exec: cp " + fromPath + " no servidor " + fromServer + " - e url : " + fromIsURL + " para " +
                    toPath + " no servidor " + toServer +" - e url : " + toIsURL);
            return false;
        }
    }

    private void copy(String pathFrom, String pathTo) throws IOException{
        File to = new File (pathTo);
        File from = new File (pathFrom);
        Files.copy(from.toPath(), to.toPath());
    }


    protected void doit() throws IOException {
        BufferedReader reader = new BufferedReader( new InputStreamReader( System.in));

        for( ; ; ) {
            String line = reader.readLine();
            if( line == null)
                break;
            String[] cmd = line.split(" ");
            if( cmd[0].equalsIgnoreCase("servers")) {
                String[] s = servers( cmd.length == 1 ? null : cmd[1]);

                if( s == null)
                    System.out.println( "error");
                else {
                    System.out.println( s.length);
                    for( int i = 0; i < s.length; i++)
                        System.out.println( s[i]);
                }
            } else if( cmd[0].equalsIgnoreCase("ls")) {
                String[] dirserver = cmd[1].split("@");
                String server = dirserver.length == 1 ? null : dirserver[0];
                boolean isURL = dirserver.length == 1 ? false : dirserver[0].indexOf('/') >= 0;
                String dir = dirserver.length == 1 ? dirserver[0] : dirserver[1];

                String[] res = dir( server, isURL, dir);
                if( res != null) {
                    System.out.println( res.length);
                    for( int i = 0; i < res.length; i++)
                        System.out.println( res[i]);
                } else
                    System.out.println( "error");
            } else if( cmd[0].equalsIgnoreCase("mkdir")) {
                String[] dirserver = cmd[1].split("@");
                String server = dirserver.length == 1 ? null : dirserver[0];
                boolean isURL = dirserver.length == 1 ? false : dirserver[0].indexOf('/') >= 0;
                String dir = dirserver.length == 1 ? dirserver[0] : dirserver[1];

                boolean b = mkdir( server, isURL, dir);
                if( b)
                    System.out.println( "success");
                else
                    System.out.println( "error");
            } else if( cmd[0].equalsIgnoreCase("rmdir")) {
                String[] dirserver = cmd[1].split("@");
                String server = dirserver.length == 1 ? null : dirserver[0];
                boolean isURL = dirserver.length == 1 ? false : dirserver[0].indexOf('/') >= 0;
                String dir = dirserver.length == 1 ? dirserver[0] : dirserver[1];

                boolean b = rmdir( server, isURL, dir);
                if( b)
                    System.out.println( "success");
                else
                    System.out.println( "error");
            } else if( cmd[0].equalsIgnoreCase("rm")) {
                String[] dirserver = cmd[1].split("@");
                String server = dirserver.length == 1 ? null : dirserver[0];
                boolean isURL = dirserver.length == 1 ? false : dirserver[0].indexOf('/') >= 0;
                String path = dirserver.length == 1 ? dirserver[0] : dirserver[1];

                boolean b = rm( server, isURL, path);
                if( b)
                    System.out.println( "success");
                else
                    System.out.println( "error");
            } else if( cmd[0].equalsIgnoreCase("getattr")) {
                String[] dirserver = cmd[1].split("@");
                String server = dirserver.length == 1 ? null : dirserver[0];
                boolean isURL = dirserver.length == 1 ? false : dirserver[0].indexOf('/') >= 0;
                String path = dirserver.length == 1 ? dirserver[0] : dirserver[1];

                FileInfo info = getAttr( server, isURL, path);
                if( info != null) {
                    System.out.println( info);
                    System.out.println( "success");
                } else
                    System.out.println( "error");
            } else if( cmd[0].equalsIgnoreCase("cp")) {
                String[] dirserver1 = cmd[1].split("@");
                String server1 = dirserver1.length == 1 ? null : dirserver1[0];
                boolean isURL1 = dirserver1.length == 1 ? false : dirserver1[0].indexOf('/') >= 0;
                String path1 = dirserver1.length == 1 ? dirserver1[0] : dirserver1[1];

                String[] dirserver2 = cmd[2].split("@");
                String server2 = dirserver2.length == 1 ? null : dirserver2[0];
                boolean isURL2 = dirserver2.length == 1 ? false : dirserver2[0].indexOf('/') >= 0;
                String path2 = dirserver2.length == 1 ? dirserver2[0] : dirserver2[1];

                boolean b = cp( server1, isURL1, path1, server2, isURL2, path2);
                if( b)
                    System.out.println( "success");
                else
                    System.out.println( "error");
            } else if( cmd[0].equalsIgnoreCase("help")) {
                System.out.println("servers - lista nomes de servidores a executar");
                System.out.println("servers nome - lista URL dos servidores com nome nome");
                System.out.println("ls server@dir - lista ficheiros/directorias presentes na directoria dir (. e .. tem o significado habitual), caso existam ficheiros com o mesmo nome devem ser apresentados como nome@server;");
                System.out.println("mkdir server@dir - cria a directoria dir no servidor server");
                System.out.println("rmdir server@udir - remove a directoria dir no servidor server");
                System.out.println("cp path1 path2 - copia o ficheiro path1 para path2; quando path representa um ficheiro num servidor deve ter a forma server:path, quando representa um ficheiro local deve ter a forma path");
                System.out.println("rm path - remove o ficheiro path");
                System.out.println("getattr path - apresenta informacao sobre o ficheiro/directoria path, incluindo: nome, boolean indicando se e ficheiro, data da criacao, data da ultima modificacao");
            } else if( cmd[0].equalsIgnoreCase("exit"))
                break;

        }
    }

    public static void main( String[] args) {
        if( args.length != 1) {
            System.out.println("Use: java trab1.FileClient URL");
            return;
        }
        try {
            new FileClient( args[0]).doit();
        } catch (IOException e) {
            System.err.println("Error:" + e.getMessage());
            e.printStackTrace();
        }
    }

}