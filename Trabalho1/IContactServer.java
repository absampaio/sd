package Trabalho1;

import java.rmi.*;
import java.util.*;

public interface IContactServer extends Remote  {

    public void register(String name, String address) throws RemoteException;

    public String[] getServers(String name) throws RemoteException, InfoNotFoundException;

}
