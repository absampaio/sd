package Trabalho1;

import java.rmi.*;
import java.util.*;

public interface IFileServer extends Remote
{
	/**
	 * Lista nome de ficheiros num dado directorio
	 */
	public String[] dir( String path) throws RemoteException, InfoNotFoundException;
	
	/**
	 * Devolve informacao sobre ficheiro.
	 */
	public FileInfo getFileInfo( String path) throws RemoteException, InfoNotFoundException;

    public FileProper getFile( String path) throws RemoteException, InfoNotFoundException;
    
    public void makeDir(String path) throws RemoteException, InfoNotFoundException;
    
    public void removeDir(String path) throws RemoteException, InfoNotFoundException;
    
    public void removeFile(String path) throws RemoteException, InfoNotFoundException;
    
    public String [] listFiles(String path) throws RemoteException, InfoNotFoundException;

    public void putFile( String path, byte[] data) throws RemoteException, InfoNotFoundException;
    
    public String getName() throws RemoteException;


}
