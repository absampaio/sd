package Trabalho1;

import java.net.InetAddress;
import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.util.*;

public class ContactServer extends UnicastRemoteObject implements IContactServer {

    private static final long serialVersionUID = 1L;

    private Map<String,List<String>> serverList;


    protected ContactServer() throws RemoteException {
        super();
        serverList = new HashMap<String,List<String>>();
    }

    public void register(String name, String address) throws RemoteException{
        List<String> servers = serverList.get(name);
        if(servers == null) {
            servers = new ArrayList<String>();
            servers.add(address);
            serverList.put(name,servers);
        }
        else {
            servers.add(address);
        }
        System.out.println("File Server with name " + name + " registered at " +address);
    }

    public String[] getServers(String name) throws RemoteException, InfoNotFoundException{
        String[] result;
        if(name == null) {
            result = new String[serverList.size()];
            Set<String> names = serverList.keySet();
            int n = 0;
            for(String address: names) {
                result[n] = address;
                n++;
            }
        }
        else {
            List<String> servers = serverList.get(name);

            if(servers == null) throw new InfoNotFoundException("No server with that name");
            else {
                result = new String[servers.size()];
                int n = 0;
                for(String address: servers) {
                    result[n] = address;
                    n++;
                }
            }
        }
        return result;
    }

    public void checkServerList() {
        System.out.println("Checking file servers...");
        try {
            String[] names = getServers(null);
            for(String name: names) {
                List<String> addresses = serverList.get(name);
                for(int n = 0; n < addresses.size(); n++) {
                    String address = addresses.get(n);
                    try{
                        IFileServer server = (IFileServer) Naming.lookup("//" + address + "/" + name);
                        System.out.println(server.getName()+" at "+address+" is online");
                    } catch (Exception e) {
                        addresses.remove(address);
                        if(addresses.isEmpty()) serverList.remove(name);
                        System.out.println("File Server "+name+" at "+address+" disconnected.");
                    }
                }
            }
            System.out.println("Check Done.");
            Thread.sleep(10000);
            checkServerList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main( String args[]) throws Exception {
        try {
            if( args.length != 1){
                System.out.println("USE: server_name");
                System.exit(1);
           } 

            System.getProperties().put( "java.security.policy", "src/policy.all");

            if( System.getSecurityManager() == null) {
                System.setSecurityManager( new RMISecurityManager());
            }

            try { // start rmiregistry
                LocateRegistry.createRegistry( 1099);
            } catch( RemoteException e) {
                // if not start it
                // do nothing - already started with rmiregistry
            }

            ContactServer server = new ContactServer();
            Naming.rebind( args[0], server);
            System.out.println( "Contact Server bound in registry");
            server.checkServerList();
        } catch( Throwable th) {
            th.printStackTrace();
        }
    }

}